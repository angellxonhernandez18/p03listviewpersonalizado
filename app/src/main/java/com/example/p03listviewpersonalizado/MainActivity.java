package com.example.p03listviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private ListView lst;
    private SearchView srcLista;
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lst = (ListView) findViewById(R.id.lstNombres);
        arrayList = new ArrayList<>();
// Obtén los nombres y las matrículas de los recursos
        String[] nombres = getResources().getStringArray(R.array.array_nombres);
        String[] matriculas = getResources().getStringArray(R.array.array_matricula);
        String[] imagenes = getResources().getStringArray(R.array.array_imagenes);
// Combina los nombres y las matrículas en una lista
        for (int i = 0; i < nombres.length; i++) {
            arrayList.add(nombres[i] + " - " + matriculas[i]);
        }
        adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_expandable_list_item_1, arrayList);
        lst.setAdapter(adapter);

        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nombreMatricula = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(MainActivity.this, "Seleccionó: " + nombreMatricula, Toast.LENGTH_SHORT).show();
            }
        });}}

